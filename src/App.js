import React, { Component } from 'react';
import HelloWorld from './components/HelloWorld';
import HandleInput from './components/HandleInput';
import InternalJson from './components/InternalJson';

import ExternalJson from './components/ExternalJson';

class App extends Component {
  render() {
    return(
      <div>
        <HelloWorld nama="Mohamad Arif Mujaki"/>
        <HelloWorld nama="Nuruf Fauziah"/>
        <HelloWorld nama="Nura Maryam Assyfa"/>
        <HandleInput />
        <InternalJson />
        <ExternalJson />
      </div>
    );
  }
}

export default App;