import React, { Component } from 'react';
import '../App.css';

export default class ExternalJson extends Component {
  constructor() {
    super();
    this.state = {
      homeStays: []
    }
  }

  componentDidMount() {
    fetch("https://raw.githubusercontent.com/algosigma/js-reactjs/master/homestays.json")
    .then(response => response.json())
    .then(data => {
      this.setState({
        homeStays: data
      })
    })
  }
  render() {
    return (
      <div className="warna">
        {
          this.state.homeStays.map((homeStay, key) =>
            <div>
              <h3>{homeStay.nama} - Rp. {homeStay.harga}</h3>
            </div>
          )
        }
      </div>
    )
  }
}
