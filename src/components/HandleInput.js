import React, { Component } from 'react';
import '../App.css';

class HandleInput extends Component {

  constructor() {
    super();
    this.state = {
      data : ''
    }
  }

  handleChange = (event) => {
    this.setState({
      data : event.target.value
    })
  }
  render() {
    return (
      <div className="warna">
        <p>Please type any sentence below. </p>
        <input type="text" onChange={this.handleChange} />
        <h3>{this.state.data}</h3>
      </div>
    );
  }
}

export default HandleInput;
