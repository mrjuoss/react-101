import React, { Component } from 'react';
import '../App.css';

export default class InternalJson extends Component {
  constructor() {
    super();
    this.state = {
      students: [
        {
          id: 1,
          nama: 'Mohamad',
        },
        {
          id: 2,
          nama: 'Arif',
        },
        {
          id: 3,
          nama: 'Mujaki',
        },
        {
          id: 4,
          nama: 'Subangkit',
        },
        {
          id: 5,
          nama: 'Achmad',
        },
        {
          id: 6,
          nama: 'Husen',
        }
      ]
    }
  }

  render() {
    return (
      <div class="warna">
        {
          this.state.students.map((student, key) =>
          <div>
            Key : {key} - ID: {student.id} - Nama : {student.nama}
          </div>
          )
        }
      </div>
    )
  }
}
