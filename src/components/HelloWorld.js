import React, { Component } from 'react';
import '../App.css';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      salam: 'Halo'
    }
  }

  sayHai = () => {
    this.setState({
      salam: 'Salam untukmu'
    });
  }
  render() {
    return (
      <div className="warna">
        <p>
          {this.state.salam}
        </p>
        <p>
          {this.props.nama}, Selamat Beajar ReactJS 101
        </p>
        <button onClick={this.sayHai}>
          Salamkan.
        </button>
      </div>
    );
  }
}

export default App;